var config;
var error;
$(document).ready(function(){
	$("input")[0].focus();
	var currentBoxNumber = 0;
	$("input").keyup(function (event) {
	    if (event.keyCode == 13) {
	        textboxes = $("input");
	        currentBoxNumber = textboxes.index(this);
	        if (textboxes[currentBoxNumber + 1] != null) {
	            nextBox = textboxes[currentBoxNumber + 1];
	            nextBox.select();
	            event.preventDefault();
	            return false;
	        }
	    }
	});
	$("#password").keyup(function(event){
	    if(event.keyCode == 13){
	        $("#change").click();
	    }
	});
    //loadConfig("current_config");
    loadConfig("saved_config");
})
function loadConfig(cfg){
    $.ajax({
        url:'/map/config?par='+cfg,
        success:function(data){
            console.log('config loaded');
            config = data;
            document.getElementById("DBName").value=data.DBName;
            document.getElementById("host").value=data.host;
            document.getElementById("port").value=data.port;
            document.getElementById("username").value=data.username;
            document.getElementById("password").value=data.password;
            document.getElementById("poolSize").value=data.poolSize;

        },
        error:function(jqXHR,status,error){
            console.log('failed to load settings');
            if(jqXHR.status==404){
                notifyFailure("Файл настроек не был найден");
            }
            if(jqXHR.status==500){
                notifyFailure("Некорректный формат файла настроек");
            }
            console.log("XHR.status "+jqXHR.status);

        }
    });
}
function onChangeBtnClick(){
	console.log('trying to connect');
    var xhr = new XMLHttpRequest();
    var body= new Object();

    body.DBName = document.getElementById("DBName").value;
    body.host = document.getElementById("host").value;
    body.port = document.getElementById("port").value;
    body.username = document.getElementById("username").value;
    body.password = document.getElementById("password").value;
		body.poolSize = +Math.round(document.getElementById("poolSize").value);
		if(body.poolSize<1){
				console.log('body.poolSize '+body.poolSize+' < 1');
				notifyFailure("Число подключений должно быть больше 0");
				return;
		}
    xhr.open("POST", '/map/config?p=change', true);
    xhr.setRequestHeader('Content-Type', 'UTF-8');
    xhr.send(JSON.stringify(body));

    xhr.onreadystatechange = function() {
      if (xhr.readyState != 4) return;

      if (xhr.status == 200) {
    	  console.log('success connection');
    	  notifySuccess("Соеденение успешно изменено");
      } else {
          console.log(xhr.status + ': ' + xhr.statusText);
          if(xhr.status==501){
              notifyCreateTables("Необходимые для работы модуля таблицы не были найдены");
          }else{
              notifyFailureWithMsg("Неудалось подключиться к базе данных с заданными настройками",xhr.responseText);
          }
      }
    }
}
function onSaveBtnClick(){
		console.log('sending config');
    var xhr = new XMLHttpRequest();
    var body= new Object();

    body.DBName = document.getElementById("DBName").value;
    body.host = document.getElementById("host").value;
    body.port = document.getElementById("port").value;
    body.username = document.getElementById("username").value;
    body.password = document.getElementById("password").value;
		body.poolSize = +Math.round(document.getElementById("poolSize").value);
		console.log(body.poolSize );
		if(body.poolSize<1){
				console.log('body.poolSize '+body.poolSize+' < 1');
				notifyFailure("Число подключений должно быть больше 0");
				return;
		}
    xhr.open("POST", '/map/config?p=save', true);
    xhr.setRequestHeader('Content-Type', 'UTF-8');
    xhr.send(JSON.stringify(body));

    xhr.onreadystatechange = function() {
      if (xhr.readyState != 4) return;

      if (xhr.status == 200) {
    	  console.log('config sent');
    	  notifySuccess("Настройки были успешно сохранены");
      } else {
    	  if(xhr.status == 409){
    		console.log(xhr.status + ': ' + xhr.statusText);
    	  	notifyFailure("Неудалось подключиться к базе данных с заданными настройками"+' : '+xhr.responseText);
    	  }else{
    		  console.log(xhr.status + ': ' + xhr.statusText);
      	  	notifyFailure("Настройки не были сохранены");
    	  }
      }
    }
}
function notifySuccess(message){
	$('.alert-box').remove();
	$('<div class="alert-box success" id="success">'+message+'</div>')
		.prependTo('body').delay(3000).fadeOut(1000, function() {
			$('#success').remove();
	});
}
function notifyFailure(message) {
	$('.alert-box').remove();
	$('<div class="alert-box failure" style="font-size:12pt;" >'
			+'<p class="message">'+message+'</p><button class="close_btn" onclick="onClose()">x</button>')
			.prependTo('body');
}
function notifyFailureWithMsg(message,err) {
	error=err;
	$('.alert-box').remove();
	$('<div class="alert-box failure"  >'
			+'<p class="message">'+message+'</p><button class="close_btn" onclick="onClose()">x</button>'
			+'<button class="show_btn" onclick="onShow()">Показать stack trace</button></div>')
			.prependTo('body');
}
function notifyCreateTables(message) {
    $('.alert-box').remove();
    $('<div class="alert-box failure"  >'
        +'<p class="message">'+message+'</p><button class="close_btn" onclick="onClose()">x</button>'
        +'<button class="show_btn" onclick="onCreateTables()">Создать необходимые таблицы</button></div>')
        .prependTo('body');
}
function onClose(){
	$('.alert-box').fadeOut(200, function() {
		$('.alert-box').remove();
	});
}
function onShow(){
    var data = "<p>"+error+"</p>";
    stackTraceWindow = window.open("data:text/html," + encodeURIComponent(data),"_blank");
    stackTraceWindow.focus();
}
function onCreateTables(){
    console.log('trying to connect');
    var xhr = new XMLHttpRequest();
    xhr.open("POST", '/map/config?p=create', true);
    xhr.setRequestHeader('Content-Type', 'UTF-8');
    xhr.send(JSON.stringify(config));
    xhr.onreadystatechange = function() {
        if (xhr.readyState != 4) return;
        if (xhr.status == 200) {
            console.log('success connection');
            notifySuccess("Необходимые таблицы были созданы");
        } else {
            console.log(xhr.status + ': ' + xhr.statusText);
            notifyFailureWithMsg("Неудалось создать необходимые таблицы",xhr.responseText);
        }
    }
}