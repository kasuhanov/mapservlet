var map=null;
var markers = new Map();
var users;
var selectedUser="";
var selectedType="1";
var clicked = [];
var houses;
var order="city";
var max=60000;
var min=0;
var imgSteps=10;
var imgStep;
var errAlerts=0;
var errMsg="";

$(document).ready(function(){
	ymaps.ready(function(){
		drawMap();
		loadUsers();
		initMap().then(function(){
            document.getElementById("user_select").disabled=false;
            document.getElementById("type_select").disabled=false;
        });
		dragAndDrop();
	});
});
function initMap() {
    var dfd = $.Deferred();
    $('#download').remove();
    $('#prog').remove();
    $('<p id="download"style="text-align: center;">Загружается информация о домах</p>')
        .appendTo('#over_map');
	$.ajax({
        url:'/map/houses?user='+encodeURIComponent(selectedUser)+'&type='+selectedType,
        success:function(data){
            $('#download').remove();
            console.log('houses loaded');
            if(data.length==0){
                console.log('data length = 0');
                notifyError('В базе данных нет домов, соответствующих заданному условию');
                document.getElementById("user_select").disabled=false;
                document.getElementById("type_select").disabled=false;
            }
            houses=data;
        },
        error:function(xhr,status,error){
            $('#download').remove();
            document.getElementById("user_select").disabled=false;
            document.getElementById("type_select").disabled=false;
        	notifyError('Неудалось загрузить информацию о домах');
        },
        timeout: 20000
    }).then(function(){
        if(houses.length==0)
            return;
        geocodeAll().then(function(){
            for (var i = 0; i < houses.length; ++i) {
                if(houses[i].location!=null)drawMarker(i);
            }
            var bounds = new google.maps.LatLngBounds();
            markers.forEach(function(value, key, map) {
                bounds.extend(value.position);
            })
            map.fitBounds(bounds);
            dfd.resolve();
        });
    });
    return dfd.promise();
}
function loadUsers(){
	$.ajax({
        url:'/map/usersjson',
        success:function(data){
            console.log('users loaded');
            users=data;
        },
        error:function(xhr,status,error){
        	notifyError('Неудалось загрузить информацию о пользователях');
        }
    }).then(function(){
        $("#user_select").append( $('<option >Показать все дома</option>'));
    	for (var i = 0; i < users.length; ++i) {
    		$("#user_select").append( $('<option value="'+users[i].UserId+'">'+users[i].User+'</option>'));
        }
    }).then(function(){
    	var select = document.getElementById('user_select')
    	if(selectedUser!=""){
    		for (var i = 0; i < select.options.length; i++) {
      		  var option = select.options[i];
      		  if(option.text==selectedUser) {
      			  option.selected=true;
      		  }
    		}
    	}
    });
}
function geocodeAll() {
    var dfd = $.Deferred();
    notifyGeo(houses.length);
    var promises = [];
    geoC(houses).then(function(){
            dfd.resolve();
            console.log('geocoding ended');
        });
    return dfd.promise();
}
function geoC(houseArr){
    var dfd_all = $.Deferred();
    var promises = $.map(houseArr, function(house){
        var dfd = $.Deferred();
        if((house.Latitude!=null)&&(house.Longitude!=null)){
            console.log('geocoding isnt necessary');
            house.location=new google.maps.LatLng(house.Latitude, house.Longitude);
            notifyGeoUpdate();
            dfd.resolve();
        }else{
            var myGeocoder = ymaps.geocode(house.Address, { results: 1 });
            myGeocoder.then(
                function (res) {
                    console.log('YaGeocoding status OK');
                    var loc = res.geoObjects.get(0).geometry.getCoordinates();
                    house.location =new google.maps.LatLng(loc[0], loc[1]);
                    sendLocation(house);
                    notifyGeoUpdate();
                    dfd.resolve();
                },
                function (err) {
                    console.log('YaGeocoding status FAIL:'+err);
                    notifyGeoUpdate();
                    dfd.resolve();
                }
            );
        }
        return dfd.promise();
    });
    $.when.apply(this, promises)
        .done(function(){
            dfd_all.resolve();
        });
    return dfd_all.promise();
}
function geocodeYandex(house){
	var myGeocoder = ymaps.geocode(house.Address, { results: 1 });
	myGeocoder.then(
    function (res) {
        console.log('YaGeocoding status OK');
        var loc = res.geoObjects.get(0).geometry.getCoordinates();
				house.location =new google.maps.LatLng(loc[0], loc[1]);
				sendLocation(house);
    },
    function (err) {
			console.log('YaGeocoding status FAIL:'+err);
    }
	);
}
function geocode(house){
	var geocoder = new google.maps.Geocoder();
	geocoder.geocode({'address': house.Address}, function(results, status) {
		if (status === google.maps.GeocoderStatus.OK) {
			house.location = results[0].geometry.location;
			console.log('geocoding status OK');
			sendLocation(house);
		} else{
			console.log('geocoding status FAIL');
		}
	});
}
function drawMarker(i){
    if(!markers.get(houses[i].location.toString())){
        var marker = new google.maps.Marker({
            index:i,
            id:houses[i].HouseId,
            content : '<div>Суммарный долг: '+houses[i].Debt.toFixed(2).toString()+'</div><div>Адрес: '+houses[i].Address+'</div>',
            map: map,
            position: houses[i].location,
            icon:getMarkerImage(i,0)
        });
        markers.set(marker.getPosition().toString(),marker);
        marker.addListener('click',function(){onMarkerClick(marker);});
        marker.addListener('mouseover',function(){
            marker.infowindow =  new google.maps.InfoWindow({
                content: marker.content,
                map: map,
                position: marker.position
            });
            marker.infowindow.open(map,this);
        });
        marker.addListener('mouseout',function(){
            marker.infowindow.close();
        });
    }else {
        var mark = markers.get(houses[i].location.toString());
        mark.id +=','+houses[i].HouseId;
        mark.content +='<div>Суммарный долг: '+houses[i].Debt.toFixed(2).toString()+'</div><div>Адрес: '+houses[i].Address+'</div>';
    }
}
function drawMap(){
	if(map==null){
        map = new google.maps.Map(document.getElementById('map'), {
            center:new google.maps.LatLng(55, 55),
            zoom:3,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.TOP_RIGHT
            }
        });
    }
}
function onMarkerClick(marker){
    var isClicked=0;
    var index=0;
    for(var i in clicked){
        if(clicked[i]==marker.id){
            isClicked=1;
            index=i;
        }
    }
    console.log('marker with id ='+marker.id+' was clicked');
    if(isClicked==0){
    	if(clicked.length==0){
        	document.getElementById("getPdfBtn").disabled=false;
        }
        clicked.push(marker.id);
        marker.setIcon(getMarkerImage(marker.index,1));
    }else{
        console.log("removing marker with id ="+marker.id);
        clicked = jQuery.grep(clicked, function(value) {
            return value != marker.id;
        });
        marker.setIcon(getMarkerImage(marker.index,0));
        if(clicked.length==0){
        	document.getElementById("getPdfBtn").disabled=true;
        }
    }
}
function getMarkerImage(i,pressed){
	var dot="";
	if(pressed==1)
		dot="-dot";
	if((max===undefined)||(min===undefined)){
		max = Math.max.apply(Math, houses.map(function(v) {
			return v.Debt;
		}));
		min = Math.min.apply(Math, houses.map(function(v) {
			return v.Debt;
		}));
	}
	step=(max - min)/imgSteps;
	var imgNum=houses[i].Debt/step;
	var roundImgNum=Math.round(imgNum);
	if(roundImgNum>imgSteps){roundImgNum=imgSteps}
	return 'img/'+roundImgNum+dot+'.png';
}
function getPdfBtnClick(){
	if(clicked.length==0) {
        return;
    }
	window.open("/map/pdf?house_ids="+clicked.toString()+"&order="+order+"&user="+encodeURIComponent(selectedUser)+"&type="+selectedType);
}
function onSelectChange(){
	var select =document.getElementById("orderSelect");
	console.log(select.options[select.selectedIndex].text+" selected");
	switch (select.options[select.selectedIndex].text) {
	  case "Город":
		  order="city";
		  break;
	  case "Улица":
		  order="street";
		  break;
	  case "Дом":
		  order="house";
		  break;
	  case "Долг":
		  order="debt";
		  break;
	}
}
function onUserSelectChange(){
	if($("#user_select :selected").text()=="Показать все дома"){
		selectedUser="";
	}else{
		selectedUser=$("#user_select :selected").text();
	}
	clicked.length=0;
	document.getElementById("getPdfBtn").disabled=true;
	clearMap();
	initMap();
	console.log("user "+selectedUser+" selected");
}
function onTypeSelectChange(){
    selectedType=$("#type_select :selected").val();
    clicked.length=0;
    document.getElementById("getPdfBtn").disabled=true;
    clearMap();
    initMap();
    console.log("type "+$("#type_select :selected").text()+" selected");
}
function sendLocation(house){
	console.log('sending location');
    var xhr = new XMLHttpRequest();
    var body= new Object();
    body.HouseId=house.HouseId;
    body.Latitude=house.location.lat();
    body.Longitude=house.location.lng();
    xhr.open("POST", '/map/houses?param=house_location', true);
    xhr.setRequestHeader('Content-Type', 'UTF-8');
    xhr.send(JSON.stringify(body));
}
function clearMap() {
	console.log('clearing map');
	markers.forEach(function(value, key, map) {
		value.setMap(null);
	})
	markers = new Map();
	houses.length = 0;
}
function dragAndDrop(){
	var left=getCookie("cardLeft");
	var top=getCookie("cardTop");
	if((left!=undefined)&&(left!=undefined)){
		$("#over_map").css( {
			"left" : left,
			"top" : top
		});
	}
	$("#over_map").draggable({
		containment: $('#wrapper'),
	    stop: function(event, ui) {
	    	var options=new Object();
	    	options.expires=360000;
	    	setCookie("cardLeft",ui.position.left,options);
	    	setCookie("cardTop",ui.position.top,options);
	    }
	});
}
function notifyError(message){
	if(message!=errMsg)errAlerts++;
	if(errAlerts==1){
		$('<div class="alert-box" id="not">'+message+'</div>')
			.prependTo('body').delay(3000).fadeOut(1000, function() {
				$('.alert-box').remove();
				errAlerts--;
				if(errAlerts!=0){
					notifyError(errMsg);
				}
		});
	}else{
		errMsg=message;
	}
}
function notifyGeo(max){
    $('#prog').remove();
    $('<div id="prog"><p id="percent"style="text-align: center;">Геокодинг: 0 %</p><progress id="geo_progress" value="0" max="'+max+'"><div>')
      .appendTo('#over_map');
}
function notifyGeoUpdate(){
    var progressbar = $('#geo_progress');
    var max = progressbar.attr('max');
    var value = progressbar.val();
    progressbar.val(++value);
    $('#percent').text('Геокодинг: '+Math.round(value*100/max)+' %');
    if(value==max){
            $('#prog').remove();
    }
}