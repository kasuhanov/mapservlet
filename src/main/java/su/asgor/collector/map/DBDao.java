package su.asgor.collector.map;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Properties;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.json.JSONArray;
import org.json.JSONObject;

public class DBDao {
    private static DataSource dataSource;
    public static boolean isInitialized=false;
    public static String host ="";
    public static String port ="";
    public static String DBName="";
    public static String username ="";
    public static String password ="";
    public static String poolSize ="";
    static{
    	try {
			if(!DBDao.isInitialized){
				DBDao.init();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
	private static void init(){
    	InputStream fis=null;
        try{
            Properties props = new Properties();
            fis = DBDao.class.getResourceAsStream("dbconfig.xml");
            props.loadFromXML(fis);
            DBDao.host = props.getProperty("host");
            DBDao.port = props.getProperty("port");
            DBDao.DBName=props.getProperty("data_base_name");
            DBDao.username = props.getProperty("username");
            DBDao.password = props.getProperty("password");
            DBDao.poolSize = props.getProperty("poolSize");

            String connectionString="jdbc:postgresql://"+props.getProperty("host")+":"+props.getProperty("port")+"/"+props.getProperty("data_base_name");
            String user=props.getProperty("username");
            String password=props.getProperty("password");
            int poolSize=Integer.valueOf(props.getProperty("poolSize"));
            PoolProperties pp = new PoolProperties();
            pp.setDriverClassName("org.postgresql.Driver");
            pp.setUrl(connectionString);
            pp.setUsername(user);
            pp.setPassword(password);
            pp.setInitialSize(poolSize);
            pp.setMaxIdle(poolSize);
            pp.setMaxActive(poolSize);
            DataSource ds = new DataSource(pp);
            ds.getConnection().close();
            isInitialized=true;
            dataSource = ds;
        }catch(SQLException e){
        	isInitialized=false;
        	e.printStackTrace(System.err);
        	System.err.println();
            System.err.println("SQLState: " + e.getSQLState());
            System.err.println("Error Code: " + e.getErrorCode());
			System.err.println("Message : " + e.getMessage());
			if(e.getSQLState()!=null){
				if(e.getSQLState().equals("28P01")){
					System.err.println("invalid password or username");
				}
				if(e.getSQLState().equals("08001")){
					System.err.println("invalid hostname or port");
				}
				if(e.getSQLState().equals("3D000")){
					System.err.println("database does not exist");
				}
				if(e.getSQLState().equals("53300")){
					System.err.println("too many connections");
				}
			}
        }catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }catch (IOException e) {
            System.err.println(e.getMessage());
        }finally {
        	try {
        		if (fis!=null){
    				fis.close();
    			}
			} catch (Exception e2) {
				 System.err.println(e2.getMessage());
			}
		}
    } 
	public static void changeConnection(String host, String portNumber, String dbname, String user, String password, int poolSize)
			throws SQLException,ClassNotFoundException{
		try{
			if(dataSource!=null)
				dataSource.close();
            DBDao.host = host;
            DBDao.port = portNumber;
            DBDao.DBName= dbname;
            DBDao.username = user;
            DBDao.password = password;
            DBDao.poolSize = String.valueOf(poolSize);
			dataSource=null;
            PoolProperties pp = new PoolProperties();
            pp.setDriverClassName("org.postgresql.Driver");
            pp.setUrl("jdbc:postgresql://"+host+":"+portNumber+"/"+dbname);
            pp.setUsername(user);
            pp.setPassword(password);
            pp.setInitialSize(poolSize);
            pp.setMaxIdle(poolSize);
            pp.setMaxActive(poolSize);
            DataSource ds = new DataSource(pp);
            ds.getConnection().close();
            isInitialized=true;
            dataSource = ds;
        } catch (SQLException e) {
        	isInitialized=false;
        	e.printStackTrace(System.err);
            System.err.println("SQLState: " + e.getSQLState());
            System.err.println("Error Code: " + e.getErrorCode());
			System.err.println("Message : " + e.getMessage());
            throw e;
        } 
    }
	public static void connect(String host, String portNumber, String dbname, String user, String password)
		throws SQLException,ClassNotFoundException{
		try{
			Class.forName("org.postgresql.Driver");
			DriverManager.getConnection("jdbc:postgresql://"+host+":"+portNumber+"/"+dbname,user,password).close();
        } catch (SQLException e) {
        	e.printStackTrace(System.err);
            System.err.println("SQLState: " + e.getSQLState());
            System.err.println("Error Code: " + e.getErrorCode());
			System.err.println("Message : " + e.getMessage());
            throw e;
        }
	}
	public static Connection getDBConnection() throws SQLException{
		return dataSource.getConnection();
	}
	public static JSONArray getUsers() throws SQLException {
    	String selectTableSQL="SELECT id, user_name FROM users ";
    	Connection connection = null;
        Statement statement = null;
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject;
        try {
        	connection = getDBConnection();
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(selectTableSQL);
            while (rs.next()) {
                jsonObject = new JSONObject();
                jsonObject.put("User", rs.getString("user_name"));
                jsonObject.put("UserId", rs.getInt("id"));
                jsonArray.put(jsonObject);
            }
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}finally {
			try {
				if(statement!=null)
					statement.close();
				if(connection!=null)
					connection.close();
			} catch (SQLException e) {
                e.printStackTrace();
                throw e;
            }
		}
        return jsonArray;
    }
	public static JSONArray getHousesForUser(String user,String type) throws SQLException{
        String query;
        query = "SELECT house.id,house.house,street.street,city.city,lat,lng,sum(service.saldosm) as debt FROM house\n" +
            "  JOIN address ON house.id = address.house_id\n" +
            "  JOIN dispersion ON address.id = dispersion.addr\n" +
            "  JOIN users ON users.id = dispersion.usr\n" +
            "  JOIN account ON address.id = account.addr_id\n" +
            "  JOIN service ON account.id = service.account\n" +
            "  JOIN street ON street.id = house.street_id\n" +
            "  JOIN city ON city.id = street.city_id\n" +
            "  LEFT JOIN house_geo ON house.id = house_geo.house_id\n" +
            "WHERE rest=FALSE AND user_name =? AND dispersion.type = ? \n" +
            "GROUP BY house.id,street.street,city.city,lat,lng\n" +
            "ORDER BY debt;";
    	Connection connection = null;
        PreparedStatement statement = null;
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject;
        try {
        	connection = getDBConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, user);
            if(!type.equals("0"))statement.setInt(2, Integer.parseInt(type));
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                jsonObject = new JSONObject();
                jsonObject.put("Address", rs.getString("city")
                        +" "+rs.getString("street")
                        +" "+rs.getString("house"));
                if((rs.getDouble("lat")!=0)&&(rs.getDouble("lng")!=0)){
                	jsonObject.put("Latitude", rs.getDouble("lat"));
                	jsonObject.put("Longitude", rs.getDouble("lng"));
                }
                jsonObject.put("HouseId", rs.getInt("id"));
                jsonObject.put("Debt", rs.getDouble("debt"));
                //jsonObject.put("Type", rs.getInt("type"));
                jsonArray.put(jsonObject);
            }
		} catch (SQLException e) {
			e.printStackTrace();
            throw e;
		}finally {
			try {
				if(statement!=null)
					statement.close();
				if(connection!=null)
					connection.close();
			} catch (SQLException e) {
                e.printStackTrace();
                throw e;
			}
		}
        return jsonArray;
    }
	public static JSONArray getHousesJson(String type) throws SQLException{
        String selectTableSQL;
        selectTableSQL = "SELECT house.id,house.house,street.street,city.city,lat,lng,sum(service.saldosm) as debt FROM house\n" +
            "  JOIN address ON house.id = address.house_id\n" +
            "  JOIN dispersion ON address.id = dispersion.addr\n" +
            "  JOIN account ON address.id = account.addr_id\n" +
            "  JOIN service ON account.id = service.account\n" +
            "  JOIN street ON street.id = house.street_id\n" +
            "  JOIN city ON city.id = street.city_id\n" +
            "  LEFT JOIN house_geo ON house.id = house_geo.house_id\n" +
            "WHERE rest=FALSE AND type = " + type+" "+
            "GROUP BY house.id,street.street,city.city,lat,lng\n" +
            "ORDER BY debt";
    	Connection connection = null;
        Statement statement = null;
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject;
        try {
        	connection = getDBConnection();
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(selectTableSQL);
            while (rs.next()) {
                jsonObject = new JSONObject();
                jsonObject.put("Address", rs.getString("city")
                        +" "+rs.getString("street")
                        +" "+rs.getString("house"));
                if((rs.getDouble("lat")!=0)&&(rs.getDouble("lng")!=0)){
                	jsonObject.put("Latitude", rs.getDouble("lat"));
                	jsonObject.put("Longitude", rs.getDouble("lng"));
                }
                jsonObject.put("Debt", rs.getDouble("debt"));
                jsonObject.put("HouseId", rs.getInt("id"));
                //jsonObject.put("Type", rs.getInt("type"));
                jsonArray.put(jsonObject);
            }
		} catch (SQLException e) {
            e.printStackTrace();
            throw e;
		}finally {
			try {
				if(statement!=null)
					statement.close();
				if(connection!=null)
					connection.close();
			} catch (SQLException e) {
                e.printStackTrace();
                throw e;
			}
		}
        return jsonArray;
    }
	public static void addLocation(int id, double lat, double lng) throws SQLException{
    	Connection connection = null;
		PreparedStatement preparedStatement = null;
    	String updateTableSQL = "INSERT INTO house_geo(house_id, lat, lng) VALUES (?,?,?)";
    	try {
    		connection = getDBConnection();
			preparedStatement = connection.prepareStatement(updateTableSQL);
			preparedStatement.setDouble(2, lat);
			preparedStatement.setDouble(3, lng);
			preparedStatement.setInt(1, id);
			preparedStatement.execute();
		} catch (SQLException e) {
            e.printStackTrace();
            throw e;
		} finally {
			try {
				if(preparedStatement!=null)
					preparedStatement.close();
				if(connection!=null)
					connection.close();
			} catch (SQLException e) {
                e.printStackTrace();
                throw e;
			}
		}
    }
    public static ArrayList<HashMap<String, String>> getHousesArrayById(String ids,String order,String type,String user) throws SQLException{
    	Connection connection = null;
    	PreparedStatement statement = null;
        String selectSQL;
        selectSQL = "SELECT * FROM (SELECT house.id,house.house,street.street,city.city,lat,lng,sum(service.saldosm) as debt FROM house\n" +
            "  JOIN address ON house.id = address.house_id\n" +
            "  JOIN dispersion ON address.id = dispersion.addr\n" +
            "  JOIN users ON users.id = dispersion.usr\n" +
            "  JOIN account ON address.id = account.addr_id\n" +
            "  JOIN service ON account.id = service.account\n" +
            "  JOIN street ON street.id = house.street_id\n" +
            "  JOIN city ON city.id = street.city_id\n" +
            "  LEFT JOIN house_geo ON house.id = house_geo.house_id\n" +
            "WHERE rest = FALSE AND type ="+type+" ";
        if(!user.equals("")){
            selectSQL+=" AND users.user_name = '"+user+"' ";
        }
        selectSQL+="GROUP BY house.id,street.street,city.city,lat,lng\n" +
            "ORDER BY debt) as a\n" +
            "WHERE a.id in (" + ids + ")";

		if(order.equals("city")){
			selectSQL+=" ORDER BY city, street, house, debt";
		}
		if(order.equals("street")){
			selectSQL+=" ORDER BY street, city, house, debt";
		}
		if(order.equals("house")){
			selectSQL+=" ORDER BY house, city, street, debt";
		}
        if(order.equals("debt")){
			selectSQL+=" ORDER BY debt, city, street, house";
		}
        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
    	HashMap<String, String> map ;
    	try {
    		connection = getDBConnection();
    		statement = connection.prepareStatement(selectSQL);
    		ResultSet rs =statement.executeQuery();
    		while (rs.next()) {
    			map = new HashMap<String, String>();
    			map.put("City name", rs.getString("city"));
    			map.put("Street name", rs.getString("street"));
    			map.put("House name", rs.getString("house"));
    			map.put("Debt", String.format(Locale.US,"%.2f",rs.getDouble("debt")));
    			list.add(map);
    		}
		} catch (SQLException e) {
            e.printStackTrace();
            throw e;
		} finally {
			try {
				if(statement!=null)
					statement.close();
				if(connection!=null)
					connection.close();
			} catch (SQLException e) {
                e.printStackTrace();
                throw e;
			}
		}
        return list;
    }
	public static void insertTestStreets(){
    	Connection connection = null;
		PreparedStatement preparedStatement = null;
		
    	String updateTableSQL = "INSERT INTO street(id, city_id, street)"
    							+"VALUES(?,1,?)";
    	try {
    		connection = getDBConnection();
			preparedStatement = connection.prepareStatement(updateTableSQL);
			ArrayList<String> streets = new ArrayList<String>();
			streets.add("Свободы");
			streets.add("Цвиллинга");
			streets.add("Воровского");
			streets.add("Комарова");
			streets.add("Героев Танкограда");
			streets.add("Комсомольский");
			streets.add("Российская");
			streets.add("Металлургов");
			streets.add("Богдана Хмельницкого");
			streets.add("60 летия Октября");
			streets.add("Сталеваров");
			streets.add("Коммунны");
			streets.add("Черкасская");
			streets.add("Братьев Кашириных");
            int i = 6;
			for(String street:streets){
                preparedStatement.setInt(1,i);
				preparedStatement.setString(2, street);
				preparedStatement.executeUpdate();
                i++;
			}
		} catch (SQLException e) {

			System.err.println(e.getMessage());

		} finally {
			try {
				if(preparedStatement!=null)
					preparedStatement.close();
				if(connection!=null)
					connection.close();
			} catch (SQLException e) {
				System.err.println(e.getMessage());	
			}
		}
        
    }
	public static void insertTestHouses(){
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        String updateTableSQL = "INSERT INTO house(id, street_id, house, moving_user_id) "
                +"VALUES(?,?,?, 1)";
        try {
            connection = getDBConnection();
            preparedStatement = connection.prepareStatement(updateTableSQL);
            int k = 126;
            for(int i=6;i<19;i++){
                for(int j=0;j<100;j++){
                    preparedStatement.setInt(1,k);
                    preparedStatement.setInt(2, i);
                    preparedStatement.setString(3, String.valueOf(j));
                    preparedStatement.executeUpdate();
                    k++;
                }
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            try {
                if(preparedStatement!=null)
                    preparedStatement.close();
                if(connection!=null)
                    connection.close();
            } catch (SQLException e) {
                System.err.println(e.getMessage());
            }
        }
    }
    public static void insertTestAddress(){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        String updateTableSQL = "INSERT INTO address(id, house_id, flat, nonliquid, moving,has_phone,communal) "
                +"VALUES(?,?,?, FALSE, FALSE, FALSE, FALSE)";
        try {
            connection = getDBConnection();
            preparedStatement = connection.prepareStatement(updateTableSQL);
            int k = 1304;
            for(int i=126;i<521;i++){
                for(int j=1;j<30;j++){
                    preparedStatement.setInt(1,k);
                    preparedStatement.setInt(2, i);
                    preparedStatement.setString(3, String.valueOf(j));
                    preparedStatement.executeUpdate();
                    k++;
                }
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            try {
                if(preparedStatement!=null)
                    preparedStatement.close();
                if(connection!=null)
                    connection.close();
            } catch (SQLException e) {
                System.err.println(e.getMessage());
            }
        }
    }
    public static void insertTestDispersion(){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        System.out.println("insert");
        String updateTableSQL = "INSERT INTO dispersion(id,type,usr,addr,rest) "
                +" VALUES (?,?,?,?,FALSE);";
        try {
            connection = getDBConnection();
            preparedStatement = connection.prepareStatement(updateTableSQL);
            int j=590;
            int min = 1;
            int max = 5;
            for(int i=1303;i<11165;i++){
                preparedStatement.setInt(1, j);
                max=5;
                preparedStatement.setInt(2, min + (int)(Math.random() * ((max - min) + 1)));
                max =2;
                preparedStatement.setInt(3, min + (int)(Math.random() * ((max - min) + 1)));
                preparedStatement.setInt(4, i);
                preparedStatement.executeUpdate();
                j++;
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            try {
                if(preparedStatement!=null)
                    preparedStatement.close();
                if(connection!=null)
                    connection.close();
            } catch (SQLException e) {
                System.err.println(e.getMessage());
            }
        }
    }
    public static void insertTestService(){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        System.out.println("insert");
        String updateTableSQL = "INSERT INTO service(id,account,srvtp,saldoym,saldosm,active) "
                +" VALUES (?,?,1,1509,?,TRUE);";
        try {
            connection = getDBConnection();
            preparedStatement = connection.prepareStatement(updateTableSQL);
            int j=0;
            int min = 0;
            int max = 3000;
            for(int i=0;i<9861;i++){
                preparedStatement.setInt(1, j);
                preparedStatement.setInt(2, i);
                preparedStatement.setInt(3, min + (int)(Math.random() * ((max - min) + 1)));
                preparedStatement.executeUpdate();
                j++;
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            try {
                if(preparedStatement!=null)
                    preparedStatement.close();
                if(connection!=null)
                    connection.close();
            } catch (SQLException e) {
                System.err.println(e.getMessage());
            }
        }
    }
    public static void insertTestAccount(){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        System.out.println("insert");
        String updateTableSQL = "INSERT INTO account(id,no,principal_id,addr_id,active,wasactive,nonliquid,lawer) "
                +" VALUES (?,?,?,?,FALSE,FALSE,FALSE,FALSE);";
        try {
            connection = getDBConnection();
            preparedStatement = connection.prepareStatement(updateTableSQL);
            int j=0;
            for(int i=1303;i<11165;i++){
                preparedStatement.setInt(1, j);
                preparedStatement.setString(2, String.valueOf(i+j));
                preparedStatement.setInt(3, 19);
                preparedStatement.setInt(4, i);
                preparedStatement.executeUpdate();
                j++;
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            try {
                if(preparedStatement!=null)
                    preparedStatement.close();
                if(connection!=null)
                    connection.close();
            } catch (SQLException e) {
                System.err.println(e.getMessage());
            }
        }
    }
	public static void close(){
    	dataSource.close();
    }
	public static void checkDBContainsMapModule() throws SQLException,Exception {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM modules WHERE name='MAP'";
        try {
            connection = getDBConnection();
            preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (!resultSet.next() ) {
                throw new Exception("No map module found");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                if(preparedStatement!=null)
                    preparedStatement.close();
                if(connection!=null)
                    connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
                throw e;
            }
        }
    }
    public static void initMapModule() throws SQLException {
        Connection connection = null;
        Statement statement = null;
        String drop = "DROP TABLE IF EXISTS house_geo;";
        String insert = "INSERT INTO modules VALUES ('document','MAP',1);";
        String create = "CREATE TABLE house_geo " +
                "( house_id INT NOT NULL, " +
                "  lat DOUBLE PRECISION NOT NULL," +
                "  lng DOUBLE PRECISION NOT NULL," +
                "  CONSTRAINT fk_geo_house_id FOREIGN KEY (house_id)" +
                "  REFERENCES house(id)" +
                ");";
        try {
            connection = getDBConnection();
            connection.setAutoCommit(false);
            statement = connection.createStatement();
            statement.execute(drop);
            statement.execute(create);
            statement.execute(insert);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            e.printStackTrace();
            throw e;
        } finally {
            try {
                if(statement!=null)
                    statement.close();
                if(connection!=null)
                    connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
                throw e;
            }
        }
    }
}
