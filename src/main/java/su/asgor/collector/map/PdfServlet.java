package su.asgor.collector.map;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

//@WebServlet("/pdf")
public class PdfServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("house_ids")!=null){
			Document document = new Document();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                ArrayList<HashMap<String, String>> housesList;
                if((request.getParameterMap().containsKey("user"))&&(!request.getParameter("user").equals(""))){
                    String user = java.net.URLDecoder.decode(request.getQueryString(), "utf-8");
                    user = user.substring(user.indexOf("user")+5,user.indexOf("&type"));
                    //request.getQueryString().substring(5,request.getQueryString().indexOf("&"))
                    //user.substring(user.indexOf("user")+5)

                    housesList = DBDao.getHousesArrayById(request.getParameter("house_ids"), request.getParameter("order"), request.getParameter("type"),user);
                }
            	else {
                    housesList = DBDao.getHousesArrayById(request.getParameter("house_ids"), request.getParameter("order"), request.getParameter("type"),"");
                }
                PdfWriter.getInstance(document, baos);
				document.open();
				//"/WEB-INF/classes/fonts/times.ttf","/WEB-INF/classes/fonts/timesbd.ttf"
				//BaseFont bf = BaseFont.createFont(this.getServletContext().getRealPath("../resources/fonts/times.ttf"),
				//		BaseFont.IDENTITY_H,BaseFont.NOT_EMBEDDED);
                BaseFont bf = BaseFont.createFont(this.getServletContext().getRealPath("/WEB-INF/classes/fonts/times.ttf"),
                        BaseFont.IDENTITY_H,BaseFont.NOT_EMBEDDED);
				BaseFont bold = BaseFont.createFont(this.getServletContext().getRealPath("/WEB-INF/classes/fonts/timesbd.ttf"),
						BaseFont.IDENTITY_H,BaseFont.NOT_EMBEDDED);
                //BaseFont bold = BaseFont.createFont(this.getServletContext().getRealPath("../resources/fonts/timesbd.ttf"),
                 //       BaseFont.IDENTITY_H,BaseFont.NOT_EMBEDDED);
				Font font = new Font(bf);
				Paragraph paragraph = new Paragraph("Выбранные адреса", new Font(bold,18));
				paragraph.setAlignment(Element.ALIGN_CENTER);
				document.add(paragraph);
				paragraph = new Paragraph("Город: "+housesList.get(0).get("City name"), new Font(bold,14));
				paragraph.setIndentationLeft(15);
				
				if((request.getParameterMap().containsKey("user"))&&(!request.getParameter("user").equals(""))){
					document.add(paragraph);
					String user = java.net.URLDecoder.decode(request.getQueryString(), "utf-8");
					paragraph = new Paragraph("Пользователь: "+user.substring(user.indexOf("user")+5,user.indexOf("&type")), new Font(bold,14));
					paragraph.setIndentationLeft(15);
					paragraph.setSpacingAfter(14);
					document.add(paragraph);
				}else{
					paragraph.setSpacingAfter(14);
					document.add(paragraph);
				}
				PdfPTable table= new PdfPTable(2);
				table.setWidthPercentage(95);
				PdfPCell cell = new PdfPCell();
				cell.setMinimumHeight(22);
				cell.setUseAscender(true);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);

				cell.setPhrase(new Phrase("Адрес", new Font(bold,14)));
				table.addCell(cell);
				cell.setPhrase(new Phrase("Суммарный долг", new Font(bold,14)));
				table.addCell(cell);
	
				cell.setPaddingLeft(7);
				cell.setPaddingRight(7);
			    for(HashMap<String, String> map:housesList){
					cell.setHorizontalAlignment(Element.ALIGN_LEFT);
					cell.setPhrase(new Phrase(map.get("Street name")+", д."+map.get("House name"), font));
					table.addCell(cell);
					cell.setPhrase(new Phrase(map.get("Debt"), font));
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					table.addCell(cell);
			    }
			    document.add(table);
			    document.close();

			    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			    Date date = new Date();
				String filename =dateFormat.format(date)+".pdf";
				response.setContentType("application/pdf");
				response.setHeader("Content-disposition","inline; filename="+filename );
	            OutputStream os = response.getOutputStream();
	            baos.writeTo(os);
	            os.close();
			}catch (SQLException e){
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                e.printStackTrace(response.getWriter());
            }catch (DocumentException e) {
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                e.printStackTrace(response.getWriter());
                e.printStackTrace();
			}
		}
	}

}
