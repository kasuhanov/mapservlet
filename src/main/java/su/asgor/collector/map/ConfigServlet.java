package su.asgor.collector.map;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

//@WebServlet( "/map" )
public class ConfigServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        if(request.getParameterMap().containsKey("par")){
        	response.setContentType("application/json");
        	PrintWriter out = response.getWriter();
        	JSONObject jsonObject = new JSONObject();
        	InputStream fis=null;
        	Properties props = new Properties();
        	try {
				if(request.getParameter("par").equals("saved_config")) {
                    fis = DBDao.class.getResourceAsStream("dbconfig.xml");
                    props.loadFromXML(fis);
                    jsonObject.put("host", props.getProperty("host"));
                    jsonObject.put("port", props.getProperty("port"));
                    jsonObject.put("DBName", props.getProperty("data_base_name"));
                    jsonObject.put("username", props.getProperty("username"));
                    jsonObject.put("password", props.getProperty("password"));
                    jsonObject.put("poolSize", props.getProperty("poolSize"));
                    out.print(jsonObject.toString());
                }
                if(request.getParameter("par").equals("current_config")) {
                    jsonObject.put("host", DBDao.host);
                    jsonObject.put("port", DBDao.port);
                    jsonObject.put("DBName", DBDao.DBName);
                    jsonObject.put("username", DBDao.username);
                    jsonObject.put("password", DBDao.password);
                    jsonObject.put("poolSize", DBDao.poolSize);
                    out.print(jsonObject.toString());
                }
			} catch (FileNotFoundException e) {
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
				e.printStackTrace(response.getWriter());
			}
        	catch (InvalidPropertiesFormatException e) {
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				e.printStackTrace(response.getWriter());
			}finally {
				if(fis!=null){
					fis.close();
				}
			}
        }else{
        	RequestDispatcher dispatcher = request.getRequestDispatcher("/config.html");
            dispatcher.forward(request, response);
        }
	}
	@Override
 	protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		StringBuffer sb = new StringBuffer();
		String line = null;
		BufferedReader reader = request.getReader();
		while ((line = reader.readLine()) != null)
			sb.append(line);
		JSONObject json= new JSONObject(sb.toString());
		if(request.getParameter("p").equals("save")){
			saveConfig(json, response);
		}
		if(request.getParameter("p").equals("change")){
            saveConfig(json, response);
	        changeConnection(json, response);
		}
        if(request.getParameter("p").equals("create")){
            createTables(json, response);
        }
	}		
	private void saveConfig(JSONObject json, HttpServletResponse response)
			throws IOException{
		InputStream fis=null;
		FileOutputStream fos=null;
    	Properties props = new Properties();
    	try {
    		props.put("poolSize", String.valueOf(json.get("poolSize")));
            props.put("host", json.get("host"));
            props.put("port", json.get("port"));
            props.put("data_base_name", json.get("DBName"));
            props.put("username", json.get("username"));
            props.put("password", json.get("password"));
            File file = new File(this.getServletContext().getRealPath("/WEB-INF/classes/su/asgor/collector/map/dbconfig.xml"));
			fos = new FileOutputStream(file);
			props.storeToXML(fos, "");
		} catch (Exception e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace(response.getWriter());
		}finally {
			try {
        		if (fis!=null){
    				fis.close();
    			}
			} catch (Exception e2) {
				 System.err.println(e2.getMessage());
			}
			try {
        		if (fos!=null){
    				fos.close();
    			}
			} catch (Exception e3) {
				 System.err.println(e3.getMessage());
			}
		}
		return;
	}
	private void changeConnection(JSONObject json, HttpServletResponse response)
			throws IOException{
    	try {
    		DBDao.changeConnection((String)json.get("host"), (String)json.get("port"), (String)json.get("DBName"),
    				(String)json.get("username"), (String)json.get("password"),json.getInt("poolSize"));
            DBDao.checkDBContainsMapModule();
		} catch (SQLException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			e.printStackTrace(response.getWriter());
		}  catch (Exception e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            if(e.getMessage().equals("No map module found")){
                response.setStatus(501);
            }
			e.printStackTrace(response.getWriter());
		}
	}
    private void createTables(JSONObject json, HttpServletResponse response)
            throws IOException{
        try {
            DBDao.initMapModule();
        } catch (SQLException e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            e.printStackTrace(response.getWriter());
        }  catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            e.printStackTrace(response.getWriter());
        }
    }
}
