package su.asgor.collector.map;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

//@WebServlet("/houses")
public class HousesServlet extends HttpServlet {
		private static final long serialVersionUID = 1L;
	 	@Override
	    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	        response.setContentType("application/json");
	        response.setCharacterEncoding("UTF-8");
	        PrintWriter out = response.getWriter();
	        if(!DBDao.isInitialized){
	        	response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				return;
			}
			try {
                if(!request.getParameterMap().containsKey("user")) {
                    out.print(DBDao.getHousesJson(request.getParameter("type")).toString());
                    return;
                }
                if(request.getParameter("user")!=""){
                    out.print(DBDao.getHousesForUser(java.net.URLDecoder.decode(request.getQueryString().substring(5,request.getQueryString().indexOf("&")), "utf-8"),request.getParameter("type")).toString());
                }else{
                    out.print(DBDao.getHousesJson(request.getParameter("type")).toString());
                }
            }catch (SQLException e){
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                e.printStackTrace(response.getWriter());
            }finally {
                out.close();
            }
	    }
	 	@Override
	 	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
		if(request.getParameter("param").equals("house_location")) {
			StringBuffer sb = new StringBuffer();
			String line = null;
			BufferedReader reader = request.getReader();
			while ((line = reader.readLine()) != null)
				sb.append(line);
			
			JSONObject json= new JSONObject(sb.toString());
			try {
				DBDao.addLocation(json.getInt("HouseId"), json.getDouble("Latitude"), json.getDouble("Longitude"));
			} catch (SQLException e) {
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                e.printStackTrace(response.getWriter());
			}
		}
	}
}
